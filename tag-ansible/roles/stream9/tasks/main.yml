---

# Externalrepos

- name: Create an external repo of RHEL 9 alpha content for bootstrapping
  koji_external_repo:
    name: "c9s-bootstrap"
    url: "http://download.eng.bos.redhat.com/brewroot/repos/rhel-9.0.0-beta-build/latest/$arch/"

- name: Create an external repo for module builds
  koji_external_repo:
    name: "c9s-build-repo"
    url: "https://kojihub.stream.rdu2.redhat.com/kojifiles/repos/c9s-build/latest/$arch/"
    state: present

# RPM tags

- name: Create the buildtools tag
  koji_tag:
    name: c9s-build-tools
    groups:
      build:
        - bash
        - bzip2
        - centos-stream-release
        - centpkg-minimal
        - coreutils
        - cpio
        - diffutils
        - findutils
        - gawk
        - glibc-minimal-langpack
        - grep
        - gzip
        - info
        - make
        - patch
        - redhat-rpm-config
        - rpm-build
        - sed
        - shadow-utils
        - tar
        - unzip
        - util-linux
        - which
        - xz
      srpm-build:
        - bash
        - centos-stream-release
        - centpkg-minimal
        - glibc-minimal-langpack
        - gnupg2
        - redhat-rpm-config
        - rpm-build
        - shadow-utils

- name: Create the toplevel tag
  koji_tag:
    name: "c9s"
    packages:
      centos-stream: "{{ allowlist_pkgs }}"

- name: Create the pending tag
  koji_tag:
    name: "c9s-pending"
    inheritance:
      - parent: "c9s"
        priority: 10

- name: Create the gate tag
  koji_tag:
    name: "c9s-gate"
    inheritance:
      - parent: "c9s"
        priority: 10

- name: Create the candidate tag
  koji_tag:
    name: "c9s-candidate"
    inheritance:
      - parent: "c9s"
        priority: 10

- name: Create the bootstrap tag
  koji_tag:
    name: "c9s-bootstrap"
    external_repos:
      - repo: "c9s-bootstrap"
        priority: 0

- name: Create the build tag
  koji_tag:
    name: "c9s-build"
    arches: "{{ arches }}"
    extra:
      mock.new_chroot: 0
      mock.package_manager: dnf
      rhpkg_dist: el9
      rpm.macro.dist: '%{!?distprefix0:%{?distprefix}}%{expand:%{lua:for i=0,9999 do print("%{?distprefix" .. i .."}") end}}.el9%{?with_bootstrap:%{__bootstrap}}'
    inheritance:
      - parent: "c9s-build-tools"
        priority: 2
      - parent: "c9s-pending"
        priority: 5

# Build targets

- name: create the candidate target
  koji_target:
    name: "c9s-candidate"
    build_tag: "c9s-build"
    dest_tag: "c9s-gate"


# Signing/Compose tags
- name: Create the compose tag
  koji_tag:
    name: "c9s-compose"
    inheritance:
      - parent: "c9s"
        priority: 10


# Module tags
- name: Create a module build tag
  koji_tag:
    name: "module-c9s-build"
    external_repos:
      - repo: "c9s-build-repo"
        priority: 0

- name: Create a modules tag
  koji_tag:
    name: "el9-modules"
    packages:
      centos-stream: "{{ allowlist_modules }}"


- name: Create a modules candidate tag
  koji_tag:
    name: "el9-modules-candidate"
    inheritance:
      - parent: "el9-modules"
        priority: 10

- name: Create a modules override tag
  koji_tag:
    name: "el9-modules-override"
    inheritance:
      - parent: "el9-modules-candidate"
        priority: 10

- name: Create a modules dev compose tag
  koji_tag:
    name: "el9-modules-dev-compose"
    inheritance:
      - parent: "el9-modules-candidate"
        priority: 10

- name: Create a modules build tag
  koji_tag:
    name: "el9-modules-build"
    inheritance:
      - parent: "el9-modules-override"
        priority: 10

- name: Create a modules gate tag
  koji_tag:
    name: "el9-modules-gate"
    inheritance:
      - parent: "el9-modules"
        priority: 10


- name: Create a modules pending tag
  koji_tag:
    name: "el9-modules-pending"
    inheritance:
      - parent: "el9-modules"
        priority: 10

# "manual" side-tags

- name: 'make the c9s-build-test-stack-gate tag'
  koji_tag:
    name: c9s-build-test-stack-gate
    arches: "{{ arches }}"
    inheritance:
      - parent: "c9s-build"
        priority: 10

- name: 'make the c9s-build-test-stack-gate target'
  koji_target:
    name: c9s-build-test-stack-gate
    build_tag: c9s-build-test-stack-gate
    dest_tag: c9s-build-test-stack-gate

# https://issues.redhat.com/browse/CS-334
- name: 'make the c9s-build-ssl3'
  koji_tag:
    name: c9s-build-ssl3
    arches: "{{ arches }}"
    extra:
      rpm.macro.dist: .ssl3
    inheritance:
      - parent: "c9s-build"
        priority: 10

- name: 'make the c9s-build-ssl3'
  koji_target:
    name: c9s-build-ssl3
    build_tag: c9s-build-ssl3
    dest_tag: c9s-build-ssl3

# https://issues.redhat.com/browse/CS-303
- name: 'make the c9s-build-ssl-stack-gate tag'
  koji_tag:
    name: c9s-build-ssl-stack-gate
    arches: "{{ arches }}"
    inheritance:
      - parent: "c9s-build"
        priority: 10

- name: 'make the c9s-build-ssl-stack-gate target'
  koji_target:
    name: c9s-build-ssl-stack-gate
    build_tag: c9s-build-ssl-stack-gate
    dest_tag: c9s-build-ssl-stack-gate

# Image tags

- name: 'make an image base tag' 
  koji_tag:
    name: "guest-c9s"
    packages:
      centos-stream: "{{ image_allowlist_pkgs }}"

- name: 'make an image candidate tag' 
  koji_tag:
    name: 'guest-c9s-candidate'
    inheritance: 
      - parent: 'guest-c9s' 
        priority: 10

- name: 'make a container build tag' 
  koji_tag:
    name: 'guest-c9s-container-build'
    arches: "{{ arches }}"
    extra:
      mock.package_manager: dnf
    inheritance: 
      - parent: 'guest-c9s-candidate' 
        priority: 10

- name: 'make an image build tag' 
  koji_tag:
    name: 'guest-c9s-image-build'
    arches: "{{ arches }}"
    extra:
      mock.package_manager: dnf
    inheritance: 
      - parent: 'guest-c9s-candidate' 
        priority: 10

- name: 'make a container target'
  koji_target:
    name: "c9s-containers"
    build_tag: "guest-c9s-container-build"
    dest_tag: "guest-c9s-candidate"

- name: 'make an image target'
  koji_target:
    name: "c9s-images"
    build_tag: "guest-c9s-image-build"
    dest_tag: "guest-c9s-candidate"

# pesign
- name: 'make a pesign build tag'
  koji_tag:
    state: present
    name: "c9s-pesign-build"
    locked: yes
    perm: admin
    arches: "{{ arches }}"
    extra:
      mock.package_manager: dnf
      rhpkg_dist: el9
      rpm.macro.dist: ".el9"

- name: 'pesign inheritance'
  koji_tag_inheritance:
    parent_tag: "c9s-build"
    child_tag: "c9s-pesign-build"
    priority: 10

- name: pesign default target
  koji_target:
    name: "c9s-candidate-pesign"
    build_tag: "c9s-pesign-build"
    dest_tag: "c9s-gate"
